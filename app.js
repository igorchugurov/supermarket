'use strict';

var fs = require('fs');
var path = require('path');
var readline = require('readline');
var Items = require('./model/Items');
var Good = require('./model/good');
var Order = require('./model/order');
var async = require('async');
var Driver = require('./model/driver')
var User = require('./model/user');

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

//**********************************Функция ициализации списков************************************

function initList(Model, filename){
	
	var initList = [];
	var saveListFunction = function(){};
	if(filename){
		saveListFunction = function(list){
			var dataPath = path.join(__dirname, filename);
			fs.writeFile(dataPath, JSON.stringify(list), function (err,res) {
				if (err) return console.log(err);            
			});
		};

		initList = require(filename);
		if(!initList){
			initList = [];
		};
	};
	var list = new Items(Model, initList, saveListFunction);
	list.showList = function(){	
		function print(item,i,sdvig){
			sdvig++;
			var s='';
			for(var t =0;t<=sdvig;t++){s+=' '}
			var str =  s+i + ') ';
			for(var key in item){
				if(key != "id"){
					if(typeof item[key] != "object"){
						str += key+':' + item[key] + '; '
					} else{						
						item[key].forEach(function(item, i){
							var str1 = print(item, i,sdvig);
							console.log(str1);
						});
					}
				}
			}
			return str;
		}	
		var sdvig =0;
		this.getList().forEach(function(item, i){
			var str= print(item, i,sdvig);	
			console.log(str)
		});
	};
	return list;
}; 

////////////////////////////////////////////////////////////////////////////////
//Инициализация списков
var orders = initList(Order, "./data/orders.json");
var goods = initList(Good, "./data/goods.json");
var drivers = initList(Driver, "./data/drivers.json");
var cart = initList(Good);
var users = initList(User, "./data/users.json")
////////////////////////////////////////////////////////////////////////////////

orders.showOrder = function(){
	this.getList().forEach(function(order, i){
		var str = i + ') Date:' + order.date + '; Status:' + order.status + '; Adress:' + order.ship;
		console.log(str)
		order.goods.forEach(function(good, i){
			var str = '     ' + i + ') Name:' + good.name + '; Price:' + good.price + '; Weight:' + good.weight + '; Amount:' + good.amount;
			console.log(str)
		});
	});
};

///////////////////////////////////////////////////////////////////////////////////
//Экспорт контроллеров
var Goodsctrl = require("./controllers/goods");
var goodsctrl = new Goodsctrl(goods);

	function addToStore(cb, edit, inercb){
		async.series([
		    function(callback){
		        rl.question("Введите название товара ", function(answer) {		 
				  callback(null,answer)
				});
		    },
		    function(callback){
		        rl.question("Введите количество товара ", function(answer) {
				  callback(null,answer)
				});
		    },
		    function(callback){
		        rl.question("Введите вес товара ", function(answer) {
				  callback(null,answer)
				});
		    },
		    function(callback){
		        rl.question("Введите цену товара ", function(answer) {
				  callback(null,answer)
				});
		    }
		],
		function(err, results) {	
			var good = {
				name:results[0],
				amount:results[1],
				weight:results[2],
				price:results[3],
				id : Date.now()
			};
			if(edit || edit == "0"){
				good.id = goods.getItemByPoss(edit).id;
				goods.updateItem(good, ["name", "amount", "weight", "price"])
			}else{
				goods.addItem(good);
			};
			cb(inercb);
		});
	};



	function deleteGood(cb){
		console.log("00 - Возврат в предыдущее меню");
		goods.showList();
		rl.question("Введите номер товара ", function(answer) {
			if(answer == "00"){
				return cb();
			};
			var i=parseInt(answer)		
			if (i<0 || i>=goods.getList().length) {
				deleteGood(cb);
			} else{	
				goods.deleteItemByPos(i);	
				deleteGood(cb);
			}
		});	
	};

	function editGood(cb){
		console.log("00 - Возврат в предыдущее меню");
		goods.showList();
		rl.question("Введите номер товара ", function(answer) {
			if(answer == "00"){
				return cb();
			};
			var i=parseInt(answer)		
			if (i<0 || i>=goods.getList().length) {
				editGood(cb);
			} else{	
				addToStore(editGood, i, cb);
			}
		});	
	};

	function clearStore(cb){
		console.log("Вы уверены ???");
		console.log("1 - НЕТ");
		console.log("2 - ДА");
		rl.question("Введите номер ответа ", function(answer) {
			if(answer == "2"){
				goods.clearList();
			};
			cb();
		});	
	};


///////////////////////////////////////////////////////////////////////////////////
//Функции для меню товаров

var goodMenu = function(){
	console.log("0 - Возврат в предыдущее меню");
	console.log("1 - Добавить товар");
	console.log("2 - Удалить товар");
	console.log("3 - Показать список товаров");
	console.log("4 - Изменить свойства");
	console.log("5 - Очистить склад");
	rl.question("Выберете действие ", function(answer) {
		if(answer == "1"){
			addToStore(goodMenu);
		}else if(answer == "2"){
			deleteGood(goodMenu);
		}else if(answer == "3"){
			goods.showList();
			goodMenu();
		}else if(answer == "4"){
			editGood(goodMenu);
		}else if(answer == "5"){
			clearStore(goodMenu)
		}else	{
			return mainMenu();
		}
		/*if(answer == "1"){
			goodsctrl.addToStore(goodMenu);
		}else if(answer == "2"){
			goodsctrl.deleteGood(goodMenu);
		}else if(answer == "3"){
			goods.showList();
			goodMenu();
		}else if(answer == "4"){
			goodsctrl.editGood(goodMenu);
		}else if(answer == "5"){
			goodsctrl.clearStore(goodMenu)
		}else	{
			return mainMenu();
		}*/
	});
};


///////////////////////////////////////////////////////////////////////////////////
//Функции для меню корзины

function addToCart(cb){
	console.log("00 - Возврат в предыдущее меню");
	goods.showList();
	rl.question("Введите номер товара ", function(answer) {
		if(answer == "00"){
			return cb();
		};
		var i=parseInt(answer)		
		if (i<0 || i>=goods.getList().length) {
			addToCart(cb);
		} else{
			var good = goods.getItemByPoss(i);
			rl.question("Введите колличество", function(answer){
				var num=parseInt(answer)
				if (num<=0 || num>good.amount){
					addToCart(cb);
				} else {
					var o = {id : good.id, amount : (good.amount-num)}
					goods.updateItem(o, ["amount"]);
					o.amount = num;
					var goodInCart = cart.getItemById(o.id);
					if(goodInCart){
						o.amount += goodInCart.amount;
						cart.updateItem(o, ["amount"])
					} else {
						o.weight = good.weight;
						o.name = good.name;
						o.price = good.price;
						cart.addItem(o);
					};
					addToCart(cb);
				}
			})	
		}
	});	
};

function clearCart(cb){
	console.log("Вы уверены ???");
	console.log("1 - НЕТ");
	console.log("2 - ДА");
	rl.question("Введите номер ответа ", function(answer) {
		if(answer == "2"){
			while(cart.getList().length !=0){
				deleteGoodFromCartByPos(cart.getList().length-1);
			};
		};
		cb();
	});	
};

function deleteGoodFromCartByPos(i){
	var goodFromCart = cart.getItemByPoss(i);
	var goodFromStore = goods.getItemById(goodFromCart.id)
	goodFromStore.amount += goodFromCart.amount;
	goods.updateItem(goodFromStore, ["amount"])	
	cart.deleteItemByPos(i);
};

function deleteGoodFromCart(cb){
	console.log("00 - Возврат в предыдущее меню");
	cart.showList();
	rl.question("Введите номер товара ", function(answer) {
		if(answer == "00"){
			return cb();
		};
		var i=parseInt(answer)		
		if (i<0 || i>=cart.getList().length) {
			deleteGoodFromCart(cb);
		} else{	
			deleteGoodFromCartByPos(i);
			deleteGoodFromCart(cb);
		}
	});	
};


function sendOrder(cb){
	console.log("Вы зарегистрированный пользователь ?");
		console.log("1 - ДА");
		console.log("2 - НЕТ");
		rl.question("Введите номер ответа ", function(answer) {
			if(answer == "1"){
				users.showList();
				rl.question("Введите номер пользователя ", function(answer) {
					var i=parseInt(answer)		
					if (i<0 || i>=users.getList().length) {
						cb();
					} else{	
						var ship = users.getItemByPoss(i).ship;
						var o = {
							_goods : cart.getList(),
							ship : ship
						};
						orders.addItem(o);
						cart.clearList();
						cb();
					}
				});
			} else if(answer == "2"){
				rl.question("Введите адрес доставки ", function(answer) {
					var o = {
						_goods : cart.getList(),
						ship : answer
					};
					orders.addItem(o);
					cart.clearList();
					cb();
				});
			} else{
				cb();
			}
		});	

	
};

var cartMenu = function(){
	console.log("0 - Возврат в предыдущее меню");
	console.log("1 - Добавить товар в корзину");
	console.log("2 - Очистить корзину");
	console.log("3 - Показать список товаров в корзине");
	console.log("4 - Удалить товар из корзины");
	console.log("5 - Отправить заказ");
	rl.question("Выберете действие ", function(answer) {		 
		if(answer == "1"){
			console.log("Добавить");
			addToCart(cartMenu);
		}else if(answer == "2"){
			clearCart(cartMenu);
		}else if(answer == "3"){
			cart.showList();
			cartMenu();
		}else if(answer == "4"){

			deleteGoodFromCart(cartMenu);
		}else if(answer == "5"){
			sendOrder(cartMenu);
		}else{
			return mainMenu();
		}
	});
};

///////////////////////////////////////////////////////////////////////////////////
//Функции для меню заказов

var changeStatus = function(cb){
	console.log("00 - Возврат в предыдущее меню");
	orders.showOrder();
	rl.question("Введите номер заказа ", function(answer){
		if(answer == "00"){
			return cb();
		};
		var i=parseInt(answer)		
		if (i<0 || i>=orders.getList().length) {
			changeStatus(cb);
		} else {
			var orderForChange = orders.getItemByPoss(i);
			if(orderForChange.status == "1"){
				console.log("00 - Возврат в предыдущее меню");
				drivers.showList();
				rl.question("Введите номер водителя ", function(answer){
					if(answer == "00"){
						changeStatus(cb);
					} else {
						var j=parseInt(answer)		
						if (j<0 || j>=drivers.getList().length) {
							changeStatus(cb);
						} else {
							var driverForShip = drivers.getItemByPoss(j);
							if(!driverForShip.busy){
								driverForShip.busy = orderForChange.id;
								drivers.updateItem(driverForShip, ["busy"]);
								orderForChange.status = "2";
								orders.updateItem(orderForChange, ["status"]);
								cb();
								//Добавить доставку
							} else {
								changeStatus(cb);
							}							
						}						
					}					
				})
			}else if(orderForChange.status == "2"){
				var driverForShip = drivers.getItemByQuerry("busy", orderForChange.id);
				driverForShip.busy = "";
				drivers.updateItem(driverForShip, ["busy"]);
				orderForChange.status = "3";
				orders.updateItem(orderForChange, ["status"]);
				cb();

			}else{
				changeStatus(cb);
			}

		} /*if(orders.getList()[i].status == "1"){	
			
		} else if(orders.getList()[i].status == "2"){

		} else{*/

		



	});
};

var orderMenu = function(){
	console.log("0 - Возврат в предыдущее меню");
	console.log("1 - Изменить статус заказа(отправить заказ на исполнение)");
	console.log("2 - Показать список заказов");
	rl.question("Выберете действие ", function(answer) {		 
		if(answer == "1"){
			changeStatus(orderMenu);
		}else if(answer == "2"){
			orders.showOrder();
			orderMenu();
		}else{
			return mainMenu();
		}
	});
};

///////////////////////////////////////////////////////////////////////////////////
//Функции для меню водителей

function deleteDriver(cb){
	console.log("00 - Возврат в предыдущее меню");
	drivers.showList();
	rl.question("Введите номер водителя ", function(answer) {
		if(answer == "00"){
			return cb();
		};
		var i=parseInt(answer)		
		if (i<0 || i>=drivers.getList().length) {
			deleteDriver(cb);
		} else{	
			drivers.deleteItemByPos(i);	
			deleteDriver(cb);
		}
	});	
};

function addDriver(cb){
	rl.question("Введите имя водителя ", function(answer) {		 
		var driver ={
			name : answer,
			id : Date.now()
		};
		drivers.addItem(driver);
		cb();
	});
};


var driverMenu = function(){
	console.log("0 - Возврат в предыдущее меню");
	console.log("1 - Добавить водителя");
	console.log("2 - Показать список водителей");
	console.log("3 - Удалить водителя");
	rl.question("Выберете действие ", function(answer) {		 
		if(answer == "1"){
			addDriver(driverMenu);
		}else if(answer == "2"){
			drivers.showList();
			driverMenu();
		}else if(answer == "3"){
			deleteDriver(driverMenu);
		}else{
			return mainMenu();
		}
	});
};

///////////////////////////////////////////////////////////////////////////////////
//Функции для меню пользователей

function deleteUser(cb){
	console.log("00 - Возврат в предыдущее меню");
	users.showList();
	rl.question("Введите номер пользователя ", function(answer) {
		if(answer == "00"){
			return cb();
		};
		var i=parseInt(answer)		
		if (i<0 || i>=users.getList().length) {
			deleteUser(cb);
		} else{	
			users.deleteItemByPos(i);	
			deleteUser(cb);
		}
	});	
};

function editUser(cb){
		console.log("00 - Возврат в предыдущее меню");
		users.showList();
		rl.question("Введите номер пользователя ", function(answer) {
			if(answer == "00"){
				return cb();
			};
			var i=parseInt(answer)		
			if (i<0 || i>=users.getList().length) {
				editUser(cb);
			} else{	
				addUser(editUser, i, cb);
			}
		});	
	};

function addUser(cb, edit, inercb){
		async.series([
		    function(callback){
		        rl.question("Введите имя пользователя ", function(answer) {		 
				  callback(null,answer)
				});
		    },
		    function(callback){
		        rl.question("Введите адрес доставки ", function(answer) {
				  callback(null,answer)
				});
		    },
		    function(callback){
		        rl.question("Введите телефон ", function(answer) {
				  callback(null,answer)
				});
		    }
		    
		],
		function(err, results) {	
			var user = {
				name:results[0],
				ship:results[1],
				phone:results[2],
				id : Date.now()
			};
			if(edit || edit == "0"){
				user.id = users.getItemByPoss(edit).id;
				users.updateItem(user, ["name", "ship"])
			}else{
				users.addItem(user);
			};
			cb(inercb);
		});
	};




var usersMenu = function(){
	console.log("0 - Возврат в предыдущее меню");
	console.log("1 - Добавить пользователя");
	console.log("2 - Показать список пользователей");
	console.log("3 - Удалить пользователя");
	console.log("4 - Редактировать пользователя");
	rl.question("Выберете действие ", function(answer) {		 
		if(answer == "1"){
			addUser(usersMenu);
		}else if(answer == "2"){
			users.showList();
			usersMenu();
		}else if(answer == "3"){
			deleteUser(usersMenu);
		}else if(answer == "4"){
			editUser(usersMenu);
		}else{
			return mainMenu();
		}
	});
};
///////////////////////////////////////////////////////////////////////////////////
//Показ данных

function showData(){
	console.log("Список клиентов -" );
	users.showList();
	console.log("Список водителей -" );
	drivers.showList();
	console.log("Список товаров -" );
	goods.showList();
	console.log("Список заказов -" );
	orders.showOrder();
	

};

///////////////////////////////////////////////////////////////////////////////////
//Меню

var mainMenu = function(){
	console.log("1 - Меню управления товарами");
	console.log("2 - Меню управления корзиной");
	console.log("3 - Меню управления заказами");
	console.log("4 - Меню управления водителями");
	console.log("5 - Меню управления пользователями");
	console.log("6 - Вывод данных");
	rl.question("Выберете действие ", function(answer){
		if(answer == "1"){				
			goodMenu();
		}else if(answer == "2"){
			cartMenu();
		}else if(answer == "3"){
			orderMenu();
		}else if(answer == "4"){
			driverMenu();
		}else if(answer == "5"){
			usersMenu();
		}else if(answer == "6"){
			showData();
			mainMenu();
		}else{				
			return;
		}
	});
};
mainMenu();