'use strict';
var async = require('async');
var readline = require('readline');

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});


var goodCtrl = function(goods){
	var self = this;
	var count = 0;
	this.addToStore = function addToStore(cb, edit, inercb){
		count++;
		console.log(count);
		async.series([
		    function(callback){
		        rl.question("Введите название товара ", function(answer) {	
		        
				  callback(null,answer)
				});
		    },
		    function(callback){
		        rl.question("Введите количество товара ", function(answer) {
		        	
				  callback(null,answer)
				});
		    },
		    function(callback){
		        rl.question("Введите вес товара ", function(answer) {
		        	
				  callback(null,answer)
				});
		    },
		    function(callback){
		        rl.question("Введите цену товара ", function(answer) {
		        	
				  callback(null,answer)
				});
		    }
		],
		function(err, results) {	
			var good = {
				name:results[0],
				amount:results[1],
				weight:results[2],
				price:results[3],
				id : Date.now()
			};
			if(edit || edit == "0"){
				good.id = goods.getItemByPoss(edit).id;
				goods.updateItem(good, ["name", "amount", "weight", "price"])
			}else{
				goods.addItem(good);
			};
			cb(inercb);
		});
	};



	this.deleteGood = function deleteGood(cb){
		console.log("00 - Возврат в предыдущее меню");
		goods.showList();
		rl.question("Введите номер товара ", function(answer) {
			rl.close();
			if(answer == "00"){
				return cb();
			};
			var i=parseInt(answer)		
			if (i<0 || i>=goods.getList().length) {
				self.deleteGood(cb);
			} else{	
				goods.deleteItemByPos(i);	
				self.deleteGood(cb);
			}
		});	
	};

	this.editGood = function editGood(cb){
		console.log("00 - Возврат в предыдущее меню");
		goods.showList();
		rl.question("Введите номер товара ", function(answer) {
			rl.close();
			if(answer == "00"){
				return cb();
			};
			var i=parseInt(answer)		
			if (i<0 || i>=goods.getList().length) {
				self.editGood(cb);
			} else{	
				self.addToStore(editGood, i, cb);
			}
		});	
	};

	this.clearStore = function clearStore(cb){
		console.log("Вы уверены ???");
		console.log("1 - НЕТ");
		console.log("2 - ДА");
		rl.question("Введите номер ответа ", function(answer) {
			rl.close();
			if(answer == "2"){
				goods.clearList();
			};
			cb();
		});	
	};
}


module.exports = goodCtrl;
