'use strict';

var Order = function(item){
	this.id = Date.now();
	this.date = Date.now();
	this.status = "1";
	this.goods = item._goods;
	this.ship = item.ship;
	this.comment=item.comment;
};

module.exports = Order;