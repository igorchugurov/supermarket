'use strict';

var good = function(prop){
	this.name = prop.name;
	this.price = prop.price;
	this.weight = prop.weight;
	this.amount = prop.amount;
	this.id = prop.id;
}

module.exports = good;
