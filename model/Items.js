'use strict';
//Item - класс объектов которые будут сохраняться в данном списке
//_items - список
//updateFunction - функция для записи
var Items = function(Item, _items, updateFunction){
	if(!_items){
		_items = [];
	}
	var _updateFunction = (!updateFunction || (updateFunction && typeof updateFunction != "function"))?function(){}:updateFunction;

	if(!Item || (Item && typeof Item != "function")){
		return;
	}
	var self = this;

	this.getItemByQuerry = function(key, val){
		for(var i=0; i<_items.length; i++){
			if(_items[i][key] == val){
				return _items[i];
			};
		};
		return false;
	};

	this.getItemById = function(id){
		for(var i=0; i<_items.length; i++){
			if(_items[i].id == id){
				return _items[i];
			};
		};
		return false;
	};
	this.getItemByPoss = function(i){
		return _items[i];
	};
	this.addItem = function(item){
		var good = new Item(item);
		_items.push(good);
		_updateFunction(_items);

	};
	this.deleteItemByPos = function(i){
		_items.splice(i, 1);
		_updateFunction(_items);
	};
	this.deleteItemById = function(id){
		for(var i=0; i<_items.length; i++){
			if(_items[i].id == id){
				return self.deleteItemByPos(i);
			};
		};
	};
	this.updateItem = function(item, prop){
		for(var i=0; i<_items.length; i++){
			if(_items[i].id == item.id){
				for(var j=0; j<prop.length; j++){
					_items[i][prop[j]]=item[prop[j]];
				};
				break;
			};
		};
		_updateFunction(_items);
	};
	this.clearList = function(){
		_items = [];
		_updateFunction(_items);
	};
	this.getList = function (){
		return _items;
	};
};


module.exports = Items;