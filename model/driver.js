'use strict';

var Driver = function(prop){
	this.name = prop.name
	this.id = Date.now();
	this.busy = false;
};

module.exports = Driver;