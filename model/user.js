'use strict';

var User = function(prop){
	this.name = prop.name;
	this.phone =prop.phone;
	this.id = Date.now();
	this.ship = prop.ship;
	this.orders=[];
};

module.exports = User;